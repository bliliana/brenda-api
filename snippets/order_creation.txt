===========================> 1) Clean:
> db.materials.drop()
> db.materialwithstocks.drop()
> db.practices.drop()

===========================> 2) Create materiales using endpoint -> Material > Crear
name: Vaso precipitado
type: Vidrio
stock: 800

name: Matraz
type: VidrioAlvssssssd34
stock: 500

name: Probeta
type: Vidrio
stock: 600

===========================> 3) Create materials with stock (need to reference the previous values)
==> 3.1) To know what values are in the DB we can always use the Material > "Get materiales" endpoint.

[
    {
        "_id": "5c93228ae961c816c398930f",
        "name": "Vaso precipitado",
        "type": "Vidrio",
        "stock": 800,
        "image": null,
        "description": null,
        "capacity": "500 ml",
        "__v": 0
    },
    {
        "_id": "5c932374e961c816c3989310",
        "name": "Probeta",
        "type": "Vidrio",
        "stock": 600,
        "image": null,
        "description": null,
        "capacity": "500 ml",
        "__v": 0
    },
    {
        "_id": "5c93239be961c816c3989311",
        "name": "Matraz",
        "type": "VidrioAlvssssssd34",
        "stock": 500,
        "image": null,
        "description": null,
        "capacity": "500 ml",
        "__v": 0
    }
]

3.2) Use the MaterialWithStock > Create materialwithstock endpoint to create the materialwithstock endpoints.

{
    "material": "5c93228ae961c816c398930f",
    "stock": "12"
}
{
    "material": "5c932374e961c816c3989310",
    "stock": "9"
}
{
    "material": "5c93239be961c816c3989311",
    "stock": "7"
}

4) Create practice using Practice > createWithMaterials

{
    "materiales": [
        {
            "material": "5c93228ae961c816c398930f",
            "stock": "8"
        },
        {
            "material": "5c93239be961c816c3989311",
            "stock": "7"
        }
    ],
    "name": "Practice quimica 1",
    "expDate": "10",
    "course": "5c8ac13ec5143d7ca46c8cee"
}

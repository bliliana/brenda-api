var OrderSchema = Schema({
    reqDate: Number,
    expDate: Number,
    user: {type: Schema.ObjectId, ref: 'User'},
    practice: {type: Schema.ObjectId, ref: 'Practice'},
    // TODO: This might not be needed ...practice already has the stock
    material: [{type: Schema.ObjectId, ref: 'Material'}]
});

var PracticeSchema = Schema({
    name: String,
    postDate: Date,
    expDate: Number,
    file: String,
    course: {type: Schema.ObjectId, ref: 'Course'},
    material: [{type: Schema.ObjectId, ref: 'MaterialWithStock'}]
});

var MaterialWithStockSchema = Schema({
    material: {type: Schema.ObjectId, ref: 'Material'},
    stock: Number
});

var MaterialSchema = Schema({
    name: String,
    description: String,
    capacity: String,
    type: String,
    stock: Number,
    image: String
});

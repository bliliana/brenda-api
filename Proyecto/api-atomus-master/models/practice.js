'use strict'
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/*
	WHATIS:
	Practice es solo una referencia por escrito que estipula qué se necesita (materiales).

	Cuántos materiales.
*/
var PracticeSchema = Schema({
    name: String,
    postDate: Date,
    expDate: Number,
    file: String,
    course: {type: Schema.ObjectId, ref: 'Course'},
    material: [{type: Schema.ObjectId, ref: 'MaterialWithStock'}]
});

module.exports = mongoose.model('Practice', PracticeSchema);
'use strict'
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var MaterialWithStockSchema = Schema({
    material: {type: Schema.ObjectId, ref: 'Material'},
    stock: Number
});

module.exports = mongoose.model('MaterialWithStock', MaterialWithStockSchema);


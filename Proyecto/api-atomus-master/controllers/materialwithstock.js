/*
    *   AUTHOR: ALONSO R
    *   DATE: 2/18/2019
    *   DESC: Class to manage materials.
    *   LICENSE: CLOSED - SOURCE
*/

'use strict'
// Imports
var fs = require('fs');
var path = require('path');

// Models:
var MaterialWithStock = require('../models/materialwithstock');
var Material = require('../models/material');

// Misc
var apiMsg = 'Server Error ... ';

function newMaterialWithStock(req, res) {
    var params = req.body;
    var materiales = params.materiales;

    if (materiales) {
        for (var i = 0; i < materiales.length; i++) {
            
            var materialQuery = materiales[i].split("-");
            var materialId = materialQuery[0];
            var materialStock = materialQuery[1];

            Material.findById(materialId, (err, found) => {
                if (err) {
                    console.log(err);
                    console.log('Error trying to find: ' + materialId);
                    res.status(500).send({message:apiMsg + ' => ' + err});
                } else {
                    var materialToSave = new MaterialWithStock();
                    materialToSave.material = found;
                    materialToSave.stock = materialStock;

                    materialToSave.save((err, saved) => {
                        if (err) {
                            res.status(500).send({message:apiMsg + ' => ' + err});
                        } else {
                            console.log("Material saved correctly ... ");
                            console.log(saved);
                            res.status(200).send(saved);
                        }
                    });
                }
            });
        }
    } else {
        res.status(400).send({message:"Inserte todos los campos."});
    }

    res.status(200).send("OK");
}

function newMaterialWithStock2(req, res) {
    var params = req.body;

    console.log(params);

    var materialWithStockToCreate = new MaterialWithStock();
    materialWithStockToCreate.material = params.material;
    materialWithStockToCreate.stock = params.stock;

    console.log(materialWithStockToCreate);

    materialWithStockToCreate.save((err, saved) => {
        if (err) {
            console.log('Returning 500 #3');
            res.status(500).send({message:apiMsg + " #2"});
        } else {
            if (!saved) {
                console.log('Returning 404 #4');
                res.status(404).send({message: 'Error al crear material with stock.'});
            } else {
                console.log('Returning 200 #5');
                res.status(200).send(saved);
            }
        }
    });
}

function getAllMaterialsWithStock(req, res) {
    MaterialWithStock.find((err, materials) => {
        if (err) {
            res.status(500).send({message:apiMsg + "#$$%"});
        } else { 
            if (!materials) {
                res.status(503).send({message:"No se encontraron materiales."});
            } else { 
                res.status(200).send(materials);
            }
        }
    });
}

module.exports = {
    newMaterialWithStock2, 
    getAllMaterialsWithStock
};
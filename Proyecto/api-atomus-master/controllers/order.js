/*
    *   AUTHOR: ALONSO R
    *   DATE: 2/19/2019
    *   DESC: Class to manage orders.
    *   LICENSE: CLOSED - SOURCE
*/

'use strict'
// Imports
var moment = require('moment');

// Model(s)
var Order = require('../models/order');
var Practice = require('../models/practice');
var User = require('../models/user');
var Material = require('../models/material');
var MaterialWithStock = require('../models/materialwithstock');
var Ban  = require('../models/ban');
var utils = require('../utils/utils');
var async = require('async');

// Misc
const apiMsg = 'Server Error.';

// Test function for UNIX's Time Stamp & momentjs 
function testExp(req, res) {
    var date = moment().unix();
    //var tmp = moment.unix(date).format('dddd, MMMM Do, YYYY h:mm:ss A');
    var tmp = moment.unix(date); 
    var exp = moment(tmp).add(2, 'days');
    console.log(moment().unix());
    res.status(200).send({time: moment(exp).add((10), 'days').toLocaleString()});
};

function testUpdate(req, res){
    var orderId = req.params.id;
    Order.findById(orderId).then(function(order) {
        var matQ = [];
      
        order.forEach(function(u) {
          matQ.push(Material.find({material:u.material}));
        });
      
        return Promise.all(matQ);
      }).then(function(list) {
          res.status(200).send(list);
      }).catch(function(error) {
          res.status(500).send('one of the queries failed', error);
      }
    ); 
}

// Creates new order
// requires: user_id, practice_id
// note: (It fills automatically order's mat list with practice's mat list).
function createOrder(req, res) {
    var order = new Order();
    var params = req.body;

    order.reqDate = moment().unix();
    // TODO: Fix this ... 
    // Need to fill up order.user with the actual object, not the param id ... 

    User.findOne({_id: params.user}, (err, userFound) => {
        if (err) {
            console.log(err);
        } else {
            console.log(':D');
            order.user = userFound;
        }
    });

    order.practice = params.practice;

    console.log('Creating order for: ' + order.user);
    console.log('With practice: ' + order.practice);

    if (order.practice) {
        Practice.findById(order.practice, (err, practice) => {
            if (err) {
                res.status(500).send({message:apiMsg});
            } else {
                if (!practice) {
                    res.status(404).send({message:"Práctica inexistente."});
                } else {
                    order.practice = practice;
                    var tmp = moment.unix(practice.expDate); 
                    order.material = practice.material;
                    order.expDate = moment(tmp).add(5, 'days').unix();

                    // User.findOne({username: order.user}, (err, user) => {
                    User.findOne( {$and: [{_id: order.user}]}, (err, found) => {
                        if (err) {
                            console.log('There was an error ... ');
                        }

                        if (found) {
                            console.log('User found: ' + found);
                        } else {
                            console.log('User not found .. ');
                        }
                    });

                    // TODO: practice.courses are not tight to the user ... so the following doesn't work ... 
                    User.findOne({$and: [{_id:order.user} /*, {course: practice.course}*/]}, (err, user) => {
                        if (err) {
                            res.status(500).send({message:apiMsg});
                        } else {
                            if (!user) {
                                console.log(order.user);
                                res.status(404).send({message:"Usuario inexistente."});
                            } else {
                                order.save((err, nOrder) => {
                                    if (err) {
                                        res.status(500).send({message:err});
                                    } else {
                                        if (!nOrder) {
                                            res.status(404).send({message:"Error al ordenar."});
                                        } else {
                                            res.status(200).send(nOrder);
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
    } else {
        res.status(400).send({message:"Campos insuficientes."});
    }
}

// Removes order
function removeOrder(req, res){
    var orderId = req.params.id;

    Order.findByIdAndRemove(orderId, (err, dOrder)=>{
        if(err){
            res.status(500).send({message:apiMsg});
        }else{
            if(!dOrder){
                res.status(404).send({message:"Error al borrar."});
            }else{
                // DELETE ON CASCADE
                // Order=>Ban
                Ban.find({order: dOrder._id}).deleteOne((err, banRemoved)=>{
                    if(err){
                        res.status(500).send({message:apiMsg});
                    }else{
                        if(!banRemoved){
                            res.status(404).send({message:"Error al borrar."});
                        }else{
                            res.status(200).send(dOrder);
                        }
                    }
                });
            }
        }
    })
}

// Get all orders populating user, materials and practice objects
function getOrders(req, res){
    var find = Order.find();

    find.populate({path: 'user'})
        .populate({path: 'practice'})
        .populate({path: 'material'})
    .exec((err, orders) => {
        if (err) {
            res.status(500).send({message:apiMsg});
        } else {
            if (!orders) {
                res.status(404).send({message:"No se encontraron órdenes."});
            } else {
                // console.log("....");
                // console.log(orders.length);
                res.status(200).send(orders);
            }
        }
    });
}

// Get order populating user, materials and practice objects
function getOrder(req, res) {

    console.log('Holixxxx');

    var orderId = req.params.id;
    var find = Order.findById(orderId);

    find.populate({path: 'user'})
        .populate({path: 'practice'})
        .populate({path: 'material'})
        .exec((err, order) => {
            if (err) {
                res.status(500).send({message:apiMsg});
            } else {
                if (!order) {
                    res.status(404).send({message:"No se encontraron órdenes."});
                } else {
                    res.status(200).send(order);
                }
            }
    });
}

// Get all user's active orders populating practice's object
// requires: user_id
function getUserOr(req, res) {
    var user = req.params.id;

    console.log('User: ' + user);
    utils.printParams(req);

    if (user) {
        // TODO: make sure that this won't affect anything ... 
        //Order.find({$and: [{user: user}, {expDate: {$gt: moment().unix()}}]})
        Order.find({user: user})
            .populate({ 
                    path: 'practice',
                    populate: {
                        path: 'material',
                        model: 'Material'
                    }
            })
            .populate({path: 'user'})
            .exec((err, orders) => {
                if (err) {
                    console.log(':( #1' + err);
                    res.status(500).send({message:apiMsg});
                } else {
                    if (!orders) {
                        console.log(':( #2');
                        res.status(404).send({message:"No se encontraron órdenes."});
                    } else {
                        console.log(':) #1');
                        console.log(orders);
                        res.status(200).send(orders);
                    }
                }
        });
    } else {
        res.status(400).send({message:"Campos insuficientes."});
    }
}

// Get all user's expired orders
// requires: user_id
function getUserExp(req, res){
    var user = req.params.id;

    if(user){
        Order.find({$and: [{user: user}, {$lte: moment().unix()}]}, (err, orders)=>{
            if(err){
                res.status(500).send({message:apiMsg});
            }else{
                if(!orders){
                    res.status(404).send({message:"No se encontraron órdenes."});
                }else{
                    res.status(200).send(orders);
                }
            }
        });
    } else {
        res.status(400).send({message:"Campos insuficientes."});
    }
}

// Creates new order and change the stock based on the materials listed in the practice
// requires: user_id, practice_id
// note: (It fills automatically order's mat list with practice's mat list).
async function createOrderChangingStock(req, res) {

    utils.printParams(req);

    var order = new Order();
    var params = req.body;
    const practiceParam = params.practice;

    await Practice.findById(practiceParam, (err, practice) => {
        if (err) {
            res.status(500).send({message: apiMsg + ' , practice: ' + practiceParam + ' not found'});
        } else {
            if (practice) {
                console.log(':DDDDD');
                order.practice = practice;
                console.log(order.practice);
                console.log('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~______________________');
                console.log(order.practice.material);
                console.log(order.practice);
                console.log(':DDDDD ;)');
            } else {
                res.status(500).send({message: apiMsg + ' , practice: ' + practiceParam + ' not found'});
            }
        }

    });

    order.reqDate = moment().unix();

    await User.findOne({_id: params.user}, (err, userFound) => {
        if (err) {
            console.log(err);
        } else {
            console.log('User found: ' + userFound);
            order.user = userFound;
        }
    });

    // order.practice = params.practice;
    var materialsWithStockFromPractice = order.practice.material;

    console.log(materialsWithStockFromPractice);

    // Iterate the materialsWithStockFromPractice and decrease from the Material model.
    for (var index = 0; index < materialsWithStockFromPractice.length; index++) {
        var materialWithStockInPractice = materialsWithStockFromPractice[index];

        console.log('Working with: ' + materialWithStockInPractice);
        console.log(materialWithStockInPractice);

        Material.findById(materialWithStockInPractice, (err, materialInDB) => {
            if (err) {
                console.log('Error trying to find: ' + materialWithStockInPractice + ' => ' + err);
                res.status(500).send({message: apiMsg + "2#$#$"});
            } else {
                if (!materialInDB) {
                    return res.status(500).send({message : 'Material no encontrado: ' + materialWithStockInPractice});
                } else {

                    MaterialWithStock.findOne({material: materialWithStockInPractice}, (err, matWithStockInDb) => {
                        if (err) {
                            res.status(500).send({message : 'Error finding material with stock ' + materialWithStockInPractice + ' ==> ' + err});
                        } else {
                            console.log(matWithStockInDb);
                            console.log('materialInDB.stock = %d ~~~~ materialWithStockInPractice.stock = %d', materialInDB.stock, matWithStockInDb.stock);
                            materialInDB.stock -= matWithStockInDb.stock;
                            console.log('materialInDB.stock = %d ~~~~ materialWithStockInPractice.stock = %d', materialInDB.stock, matWithStockInDb.stock);
                            console.log(matWithStockInDb);

                            materialInDB.save((err, materialInDBSaved) => {
                                if (err) {
                                    res.status(500).send({message : 'Error saving material: ' + materialInDB + ' ==> ' + err});
                                }
                            });
                        }
                    });

                }
            }
        });
    }

    order.save((err, orderSavedInDB) => {
        if (err) {
            res.status(500).send({message : 'Error saving order: ' + err});
        } else {
            console.log('Order saved correctly: ' + orderSavedInDB);
            res.status(200).send(orderSavedInDB);
        }
    });

}

async function markOrderAsDone(req, res) {
    console.log('Removing order ... ');
    utils.printParams(req);

    var params = req.body;
    var orderId = params.orderId;
    console.log('Order id: ' + orderId);

    await Order.findById(orderId)
        .exec((err, order) => {
            if (err) {
                res.status(500).send({message: apiMsg + ' => (order not found) ' + err});
            } else {
                if (!order) {
                    res.status(404).send({message:"No se encontraron órdenes."});
                } else {
                    
                    Practice.findById(order.practice)
                        .exec((err, practice) => {
                            if (err) {
                                res.status(500).send({message: apiMsg + ' => (practice not found) ' + err});
                            } else {

                                utils.printSeparators();
                                var materiales = practice.material;

                                console.warn('=======================================================================================================================');

                                async.each(materiales, function (materialId, err) {
                                    console.warn('Working with: %s', materialId);

                                    MaterialWithStock.findOne({material: materialId})
                                        .exec((err, materialWithStock) => {
                                            if (err) {
                                                res.status(500).send({message: apiMsg + ' => ' + err});
                                            } else {
                                                console.warn('Stock = %d, materialwithstock = %s, material = %s', materialWithStock.stock, materialWithStock, materialWithStock.material);

                                                utils.printSeparators();
                                                utils.printSeparators();
                                                console.warn(materialWithStock);
                                                console.warn('=>' + materialWithStock.material + '<=');
                                                utils.printSeparators();
                                                utils.printSeparators();

                                                Material.findOne(materialWithStock.material)
                                                    .exec((err, materialToUpdateFound) => {
                                                        if (err) {
                                                            res.status(500).send({message: apiMsg + ' => (material in order NOT found) ' + err});
                                                        } else {
                                                            utils.printSeparators2();
                                                            console.warn(materialToUpdateFound);
                                                            utils.printSeparators3();
                                                            materialToUpdateFound.stock += materialWithStock.stock;
                                                            console.warn(materialToUpdateFound);
                                                            materialToUpdateFound.save((err, materialToUpdateFoundSaved) => {
                                                                if (err) {
                                                                    res.status(500).send({message : 'Error saving material with new stock: ' + err});
                                                                } else {
                                                                    console.log('Material with new stock saved correctly ... ' + materialToUpdateFoundSaved);
                                                                }
                                                            });
                                                        }
                                                });
                                            }
                                        });

                                        console.warn("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");

                                });   
                            }
                        });

                    // Remove order:
                    Order.findByIdAndRemove(orderId, (err, removedOrder) => {
                        if (err) {
                            res.status(500).send({message: apiMsg + ' => (error removing order: ' + orderId + ') => ' + err});
                        } else {
                            res.status(200).send(removedOrder);
                        }
                    });

                }
            }
    });

}

module.exports = {
    testExp,
    testUpdate,
    createOrder,
    removeOrder,
    getOrders,
    getOrder,
    getUserOr,
    getUserExp,
    createOrderChangingStock,
    markOrderAsDone
};

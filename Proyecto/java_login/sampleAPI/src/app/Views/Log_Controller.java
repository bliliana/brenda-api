package app.Views;

import app.Services.UserService;
import com.jfoenix.controls.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;

public class Log_Controller {

    @FXML
    JFXTextField userTextField;

    @FXML
    JFXPasswordField passwordTextField;

    @FXML
    StackPane c_root;

    @FXML
    void initialize() {
        // empty for now.
    }

    @FXML
    private void getLogged(final ActionEvent event) {

        final UserService userService = new UserService();
        try {
            ArrayList<String> response = userService.awaitHash(userTextField.getText(), passwordTextField.getText());
            printResponse(response);

            if (response != null && !response.isEmpty() && response.get(0) == "200") {
                // TODO:
                final FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("Menu.fxml"));
                Parent root1 = (Parent) fxmlLoader.load();
                Stage stage = new Stage();
                // TODO: Check the following ...
                stage.setTitle("Admin Dashboard");
                stage.setScene(new Scene(root1));
                stage.show();
            } else {
                System.out.println("Error!");
                JFXDialogLayout content = new JFXDialogLayout();
                content.setHeading(new Text("Error al iniciar sesion"));
                content.setBody(new Text(response.get(1)));
                JFXDialog dialog = new JFXDialog(c_root, content, JFXDialog.DialogTransition.CENTER);
                JFXButton button = new JFXButton("OK");
                button.setOnAction(event1 -> dialog.close());
                content.setActions(button);
                dialog.show();
            }
        } catch (JSONException | IOException ex) {
            ex.printStackTrace();
        }
    }

    private void printResponse(final ArrayList<String> response) {
        response.stream().forEach(str -> System.out.println("Response: " + str));
    }


}

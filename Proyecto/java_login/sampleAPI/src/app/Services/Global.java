package app.Services;

import app.Models.User;
import app.Services.remote.APIService;
import app.Services.remote.RetrofitClient;

public class Global {

    public static String BASE_URI = "http://localhost:3977/api/";
    public static User identity;
    public static String token;
    public static String userId;

    public static APIService getAPIService(){
        return RetrofitClient.getClient(BASE_URI).create(APIService.class);
    }

    // Non-instantiable class.
    private Global() { }

}

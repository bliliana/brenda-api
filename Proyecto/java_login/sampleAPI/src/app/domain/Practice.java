package app.domain;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

public class Practice {

    private List<Material> materiales;
    private String id;
    private String name;
    private String postDate;
    private String expDate;
    private String file;
    private String courseId;

    public Practice() {
        this.materiales = new ArrayList<>();
    }

    public List<Material> getMateriales() {
        return materiales;
    }

    public void setMateriales(List<Material> materiales) {
        this.materiales = materiales;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPostDate() {
        return postDate;
    }

    public void setPostDate(String postDate) {
        this.postDate = postDate;
    }

    public String getExpDate() {
        return expDate;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    /*
    Leaving this here just in case ...
    @Override
    public String toString() {
        return "Practice{" +
                "materiales=" + materiales +
                ", id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", postDate='" + postDate + '\'' +
                ", expDate='" + expDate + '\'' +
                ", file='" + file + '\'' +
                ", courseId='" + courseId + '\'' +
                '}';
    }
    */

    @Override
    public String toString() {
        return this.name;
    }

    public static Practice fromJson(final JsonObject jsonObject) {
        final Practice practice = new Practice();
        practice.setId(jsonObject.get("_id").getAsString());
        practice.setName(jsonObject.get("name").getAsString());
        practice.setPostDate(jsonObject.get("postDate").getAsString());
        practice.setExpDate(jsonObject.get("expDate").getAsString());
        practice.setCourseId(jsonObject.get("course").getAsString());

        // Material parsing:
        final JsonArray jsonMaterials = jsonObject.getAsJsonArray("material");
        jsonMaterials.forEach(jsonMaterial -> {

            System.out.println("~~~~~~~~~~~~~~~~~~~~~>");
            System.out.println(jsonMaterial);
            System.out.println("~~~~~~~~~~~~~~~~~~~~~/>");

            final Material material = Material.fromJson(jsonMaterial.getAsJsonObject());
            practice.materiales.add(material);
        });

        return practice;
    }

}

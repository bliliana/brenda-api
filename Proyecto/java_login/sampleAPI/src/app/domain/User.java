package app.domain;

import com.google.gson.JsonObject;

public class User {

    private String id;
    private String userName;
    private String name;
    private String surName;
    private int grade;
    private String image;
    private String fullName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public static User fromJson(final JsonObject jsonObject) {
        final User user = new User();
        user.setId(jsonObject.get("_id").getAsString());
        user.setUserName(jsonObject.get("username").getAsString());
        user.setName(jsonObject.get("name").getAsString());
        user.setSurName(jsonObject.get("surname").getAsString());
        user.setGrade(jsonObject.get("grade").getAsInt());
        // user.setImage(jsonObject.get("image").getAsString());

        user.setFullName(String.format("%s %s", user.name, user.surName));

        return user;
    }

    @Override
    public String toString() {
        return this.fullName;
    }

}

package app.domain;

public class MaterialDTO {

    private final String materialName;
    private final String capacidad;
    private final String presentacion;
    private final String existencia;

    private MaterialDTO(final Builder builder) {
        this.materialName = builder.materialName;
        this.capacidad = builder.capacidad;
        this.presentacion = builder.presentacion;
        this.existencia = builder.existencia;
    }

    public static class Builder {

        private String materialName;
        private String capacidad;
        private String presentacion;
        private String existencia;

        public Builder materialName(final String materialName) {
            this.materialName = materialName;
            return this;
        }

        public Builder capacidad(final String capacidad) {
            this.capacidad = capacidad;
            return this;
        }

        public Builder presentacion(final String presentacion) {
            this.presentacion = presentacion;
            return this;
        }

        public Builder existencia(final String existencia) {
            this.existencia = existencia;
            return this;
        }

        public MaterialDTO build() {
            return new MaterialDTO(this);
        }

    }

    public String getMaterialName() {
        return materialName;
    }

    public String getCapacidad() {
        return capacidad;
    }

    public String getPresentacion() {
        return presentacion;
    }

    public String getExistencia() {
        return existencia;
    }

    @Override
    public String toString() {
        return "MaterialDTO{" +
                "materialName='" + materialName + '\'' +
                ", capacidad='" + capacidad + '\'' +
                ", presentacion='" + presentacion + '\'' +
                ", existencia='" + existencia + '\'' +
                '}';
    }
}
package app.Models;

import com.google.gson.JsonObject;
import javafx.beans.property.SimpleStringProperty;

public class Material {

    public SimpleStringProperty material = new SimpleStringProperty();
    public SimpleStringProperty capacidad = new SimpleStringProperty();
    public SimpleStringProperty presentacion = new SimpleStringProperty();
    public SimpleStringProperty existencia = new SimpleStringProperty();
    public SimpleStringProperty id = new SimpleStringProperty();

    public String getMaterial() {
        return material.get();
    }

    public String getCapacidad() {
        return capacidad.get();
    }

    public String getPresentacion() {
        return presentacion.get();
    }

    public String getExistencia() {
        return existencia.get();
    }

    public String getId() {
        return id.get();
    }

    public SimpleStringProperty idProperty() {
        return id;
    }

    public void setId(String id) {
        this.id.set(id);
    }

    @Override
    public String toString() {
        return "Material{" +
                "material=" + material.get() +
                ", capacidad=" + capacidad.get() +
                ", presentacion=" + presentacion.get() +
                ", existencia=" + existencia.get() +
                ", id=" + id.get() +
                '}';
    }

    public static Material fromJson(final JsonObject jsonObject) {
        final Material material = new Material();
        material.material.set(jsonObject.get("name").getAsString());
        material.capacidad.set(jsonObject.get("capacity").getAsString());
        material.existencia.set(jsonObject.get("stock").getAsString());
        material.presentacion.set(jsonObject.get("type").getAsString());
        material.id.set(jsonObject.get("_id").getAsString());
        return material;
    }

}


package app.utils;

import app.domain.User;
import com.google.gson.JsonObject;

public class UserUtils {

    private UserUtils() {}

    public static void fillUpUserFromJson(final User user, final JsonObject jsonObject) {
        user.setId(jsonObject.get("_id").getAsString());
        user.setUserName(jsonObject.get("username").getAsString());
        user.setName(jsonObject.get("name").getAsString());
        user.setUserName(jsonObject.get("surname").getAsString());
        user.setGrade(jsonObject.get("grade").getAsInt());
        user.setImage(jsonObject.get("image").getAsString());

        user.setFullName(String.format("%s %s", user.getName(), user.getSurName()));
    }

}
